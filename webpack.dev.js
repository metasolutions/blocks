/* eslint-disable import/no-extraneous-dependencies */
const webpack = require('webpack');
const { merge } = require('webpack-merge');
const CircularDependencyPlugin = require('circular-dependency-plugin');
const getCommonConfig = require('./webpack.common');

module.exports = (env, argv) => {
  const APP_PATH = __dirname;
  return merge(getCommonConfig(env, argv), {
    mode: 'development',
    devtool: 'inline-source-map',
    devServer: {
      hot: true,
      port: env.port || 8000,
      historyApiFallback: false,
      headers: {
        'Access-Control-Allow-Origin': '*',
      },
      client: {
        overlay: false,
      },
      static: {
        directory: APP_PATH,
      },
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new CircularDependencyPlugin({
        // exclude detection of files based on a RegExp
        exclude: /a\.js|node_modules/,
        // add errors to webpack instead of warnings
        failOnError: false,
        // allow import cycles that include an asyncronous import,
        // e.g. via import(/* webpackMode: "weak" */ './file.js')
        allowAsyncCycles: false,
        // set the current working directory for displaying module paths
        cwd: process.cwd(),
      }),
    ],
  });
};
