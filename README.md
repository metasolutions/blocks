# EntryScape Blocks 
Allows you to embed information from EntryScape in various websites or CMS:es.

See documentation on [https://docs.entryscape.com/en/blocks/](https://docs.entryscape.com/en/blocks/).

### Pre-requisites
In order to run and build Entryscape Blocks, you will need to have git, and yarn installed.  
[https://git-scm.com/](https://git-scm.com/)  
[https://yarnpkg.com](https://yarnpkg.com)

You will also need to have an Entrystore instance running somewhere (remote with CORS or locally).
You can find out more about installing entrystore at [http://entrystore.org](http://entrystore.org).

### Installing and building
Once you have cloned this repo you install all the necessary dependencies by running:
```
yarn
```

A distributable copy of Blocks can be done by running:
```
yarn build
```

The distributable files will be located in the `dist` folder.

### Running a development server
To run the development server:
```
yarn dev
```

You can access the running dev server in the browser at [http://localhost:8080](http://localhost:8080).

### Development

We use a trunk-based workflow where

- All new releases are done from the master branch
- Release of fix-versions on old releases are done in hotfix branches
- No long-lived feature branches
- New versions are released continuously

### Getting the latest deployed version

The versions are deployed at https://statisc.infra.entryscape/blocks/

So e.g. the latest version 1 is available at [https://static.infra.entryscape.com/blocks/1/app.js](https://static.infra.entryscape.com/blocks/1/app.js) and the latest version 1.1 at [https://static.infra.entryscape.com/blocks/1.1/app.js](https://static.infra.entryscape.com/blocks/1.1/app.js) etc
