/* eslint-disable import/no-extraneous-dependencies */
const { merge } = require('webpack-merge');
const TerserPlugin = require('terser-webpack-plugin');
const getCommonConfig = require('./webpack.common');

module.exports = (env, argv) => merge(getCommonConfig(env, argv), {
  mode: 'production',
  devtool: "source-map",
  optimization: {
    minimizer: [new TerserPlugin()],
  },
  plugins: [],
});
