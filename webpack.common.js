/* eslint-disable import/no-extraneous-dependencies */
const webpack = require('webpack');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const locales = ['de', 'sv', 'it'];
const momentLocaleRegExp = RegExp(locales.join('|'));

module.exports = (env, argv) => {
  const APP = 'blocks';
  const APP_PATH = __dirname;
  const showNLSWarnings = (argv && argv['nls-warnings']) || false;

  return {
    entry: './src/index.js',
    output: {
      path: path.join(__dirname, 'dist'),
      filename: 'app.js',
      chunkFilename: '[name].js',
      library: `EntryScape${APP.charAt(0).toUpperCase() + APP.substring(1)}`, // e.g EntryScapeSuite
      publicPath: 'auto'
    },
    context: APP_PATH,
    plugins: [
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        jquery: 'jquery',
//        Popper: ['popper.js', 'default'],
        m: 'mithril',
      }),
      new CopyWebpackPlugin({
        patterns: [
          {
            from: path.resolve(path.join(__dirname, 'templates', '**/*.json')),
            to: 'templates/[name][ext]',
          },
          {
            from: path.resolve(path.join(__dirname, 'assets')),
            to: 'assets', // dist/assets
          },
        ]
      }),
      new CleanWebpackPlugin(),
      new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, momentLocaleRegExp),
    ],
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules\/(?!(bootstrap|bootstrap-material-design|esi18n|@entryscape)\/).*/,
          use: [
            {
              loader: 'babel-loader',
              options: {
                presets: [['@babel/preset-env', { targets: { edge: 12 } }]],
                plugins: [
                  '@babel/plugin-proposal-object-rest-spread',
                  '@babel/plugin-proposal-class-properties',
                  '@babel/plugin-syntax-dynamic-import',
                  ['@babel/plugin-transform-react-jsx', { pragma: 'm', pragmaFrag: '\'[\'' }],
                  // Remove after upgrade of rdforms, entrystore-js, rdfjson
                  // ['@babel/plugin-transform-react-jsx', { pragma: 'm', pragmaFrag: '\'[\'' }],
                ],
              },
            },
          ],
        },
        {
          test: /\.js$/,
          resolve: {
            fullySpecified: false, // Avoid having to specify js-extension in imports
          },
        },
        {
          test: /\.nls$/,
          use: [
            {
              loader: 'nls-loader',
              options: {
                context: APP_PATH,
                showNLSWarnings,
                locales,
              },
            },
          ],
        },
        {
          test: /\.(s*)css$/,
          use: ['style-loader', 'css-loader', 'sass-loader'],
        },
        {
          test: /\.less$/,
          use: [
            'style-loader',
            'css-loader',
            {
              loader: 'less-loader',
              options: {
                strictMath: true,
              },
            },
          ],
        },
        {
          test: /\.html$/,
          type: 'asset/source',
        },
        {
          test: /\.(gif|png|jpe?g)$/i,
          type: 'asset/resource',
        },
        {
          test: /.+flag-icon-css.+\.svg$/,
          type: 'asset/resource',
          generator: {
            filename: 'flags/[folder][name][ext]',
          },
        },
        {
          test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
          exclude: /.+flag-icon-css.+\.svg$/,
          type: 'asset/resource',
          generator: {
            filename: 'fonts/[name][ext]',
          },
        },
        {
          test: /\.hbs$/,
          loader: 'handlebars-loader',
        },
      ],
    },
    resolve: {
      mainFields: ['module', 'browser', 'main'],
      mainFiles: ['index'],
      alias: {
        jquery: path.join(__dirname, 'node_modules', 'jquery'),
        commons: path.join(__dirname, 'module/commons/src'),
        catalog: path.join(__dirname, 'module/catalog/src'),
        blocks: path.join(__dirname, 'src'),
        templates: path.join(__dirname, 'templates'),
        config: path.join(__dirname, 'src', 'config', 'config'),
      },
    },
    stats: {
      warnings: false,
    },
  };
};
