#!/bin/bash
# The script will perform the following steps.
# Check on branch master or hotfix/...
# Check up to date and no local changes
#	Test Builds
#	Remove -dev from version in package.json
#	git add package.json
#	git commit -m “set release version for x.y.z”
#	git tag -a x.y.z -m “version x.y.z”
#	git push (This will trigger the build pipeline in Bitbucket of the new sharp version)
#	git push origin x.y.z
# Print version number
# Update version to x.y.(z+1)-dev in package.json
#	git add package.json
#	git commit -m “set new dev version to x.y.z-dev”
#	git push (This will trigger the build pipeline in Bitbucket of an new test version)

set -e
# Check on branch master
CURRENT_BRANCH=$(git branch --show-current)
if [ "$CURRENT_BRANCH" = "master" ] || [[ "$CURRENT_BRANCH" == hotfix/* ]]; then
    echo "OK - On branch $CURRENT_BRANCH"
else
    echo "NOK - On branch $CURRENT_BRANCH. Use master or hotfix/ branch to do a sharp deploy "
    exit 1
fi
#	no local changes
if [[ $(git status --porcelain) ]]; then
  echo "NOK - Local changes present"
  exit 1
else
  echo "OK - No local changes"
fi
#	Check up to date
git fetch
if [ "$(git log ..origin/"$CURRENT_BRANCH" --oneline | wc -l)" -eq 0 ]; then
  echo "OK - No remote changes"
else
  echo "NOK - Remote changes present"
    exit 1
fi
#	Test Builds
echo "Trying to build"
yarn
yarn build
#	Check if there is a valid "version": "x.y.z-dev" in package.json
if [ "$(grep -E '^\s*"version": "*"[0-9]+\.[0-9]+\.[0-9]+-dev",$' package.json)" ]
then
  echo "OK - Valid version in package.json"
else
  echo "NOK - Version in package.json not valid"
  exit 1
fi
# Remove -dev from version in package.json
CURRENT_DEV_VERSION=$(perl -MJSON -0777 -ne 'my $data = decode_json($_); print $data->{"version"}' package.json)
NEW_VERSION=$(echo $CURRENT_DEV_VERSION|sed -E 's/-dev//')
cat package.json | sed -E 's/(\s*"version": ")'$CURRENT_DEV_VERSION'(",)/\1'$NEW_VERSION'\2/' > package.json.new
mv package.json.new package.json
echo "New version $NEW_VERSION set in package.json"
#	Add package.json commit and push
git add package.json
git commit -m "$NEW_VERSION"
git tag -a $NEW_VERSION -m "set release version for $NEW_VERSION"
git push
git push origin $NEW_VERSION
echo "Pushed package.json"
#Print version number
echo "Released version $NEW_VERSION"
#Update version to x.y.(z+1)-dev in package.json
PATCH_VERSION=$(echo $NEW_VERSION | awk -F. '{print $3}')
MINOR_VERSION=$(echo $NEW_VERSION | awk -F. '{print $2}')
MAJOR_VERSION=$(echo $NEW_VERSION | awk -F. '{print $1}')
NEW_DEV_VERSION=$(echo $MAJOR_VERSION.$MINOR_VERSION.$((PATCH_VERSION + 1))-dev)
cat package.json | sed -E 's/(\s*"version": ")'$NEW_VERSION'(",)/\1'$NEW_DEV_VERSION'\2/' > package.json.new
mv package.json.new package.json
echo "New dev version $NEW_DEV_VERSION set in package.json"
git add package.json
git commit -m "set new dev version to $NEW_DEV_VERSION"
git push
echo "Pushed package.json"
echo "✨  Success new version $NEW_VERSION pushed for deploy."
echo "    Check Bitbucket pipeline https://bitbucket.org/metasolutions/blocks/pipelines/ for progress"