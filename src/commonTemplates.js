import dcatBundle from 'templates/dcat-ap/dcat-ap';
import geoDcatBundle from 'templates/dcat-ap/geodcat-ap';
import geoDcatVocabsBundle from 'templates/dcat-ap/geodcat-ap_vocabs';
import geoDcatPropsBundle from 'templates/dcat-ap/geodcat-ap_props';
import dcatPropsBundle from 'templates/dcat-ap/dcat-ap_props';
import dctermsBundle from 'templates/dcterms/dcterms';
import escBundle from 'templates/entryscape/esc';
import foafBundle from 'templates/foaf/foaf';
import odrsBundle from 'templates/odrs/odrs';
import skosBundle from 'templates/skos/skos';
import vcardBundle from 'templates/vcard/vcard';

export default {
  skosBundle,
  dctermsBundle,
  foafBundle,
  vcardBundle,
  odrsBundle,
  dcatPropsBundle,
  dcatBundle,
  geoDcatVocabsBundle,
  geoDcatPropsBundle,
  geoDcatBundle,
  escBundle
};