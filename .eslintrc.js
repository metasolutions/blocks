const path = require('path');

module.exports = {
  extends: ['airbnb-base', 'plugin:prettier/recommended', 'plugin:jsdoc/recommended'],
  plugins: ['jsdoc'],
  settings: {
    'import/resolver': {
      webpack: { config: path.join(__dirname, 'webpack.common.js') },
    },
  },
  env: {
    browser: true,
  },
  parser: '@babel/eslint-parser',
  parserOptions: {
    requireConfigFile: false,
    sourceType: 'module',
    allowImportExportEverywhere: true,
  },
  ignorePatterns: ['eslintrc.js'],
  rules: {
    'import/no-amd': 'off',
    // TODO remove this. use requirejs rules instead
    'no-undef': 'warn',
    //  Maybe this is better : no-underscore-dangle: [2, { 'allowAfterThis': true }]
    'no-underscore-dangle': 'off',
    'prefer-rest-params': 'off',
    'no-plusplus': ['error', { allowForLoopAfterthoughts: true }],
    'import/no-dynamic-require': 'off',
    'import/no-cycle': 'off',
    'global-require': 'off',
    'no-console': 'off',
    'no-prototype-builtins': 'off',
    'no-param-reassign': ['error', { props: false }],
    'import/extensions': ['error', { '.js': 'never' }],
    'no-await-in-loop': 'off',
    'no-restricted-syntax': ['error', 'ForInStatement', 'LabeledStatement', 'WithStatement'],
    'max-len': ['warn', { code: 120, ignoreTrailingComments: true, ignoreTemplateLiterals: true }],
    'no-use-before-define': 'warn',
    'import/prefer-default-export': 'off',
    'consistent-return': 'off',
    'prettier/prettier': [
      'error',
      {
        singleQuote: true,
        printWidth: 120,
      },
    ],
    // jsdoc
    'jsdoc/require-param-description': 'off',
    'jsdoc/require-returns-description': 'off',
  },
};
